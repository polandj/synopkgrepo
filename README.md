Synology Package Repository
============================
This is software to run a Synology Package Repository on your own server.  It is developed for use with OpenShift, but 
could easily be made to work with other environments.

Usage
-----

 - Create a new application in openshift with python and postresql.
 - Checkout the repo and then push it to your openshift application.  
 - Browse to your application's URL followed by /login and login with the username and password for your PostgreSQL database.
 - Manage your packages

The OpenShift `python` cartridge documentation can be found at:

https://github.com/openshift/origin-server/tree/master/cartridges/openshift-origin-cartridge-python/README.md
