from setuptools import setup

setup(name='SynoPkgRepo',
      version='1.0',
      description='Repository for Synology packages',
      author='Jonathan Poland',
      author_email='polandj@garble.org',
      url='http://bitbucket.org/polandj/synopkgrepo',
      install_requires=['Flask', 'MarkupSafe' , 'Flask-SQLAlchemy'],
     )
