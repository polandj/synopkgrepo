from flask import Flask, flash, request, send_from_directory, session, redirect, url_for, render_template
from flask_sqlalchemy import SQLAlchemy
from werkzeug import secure_filename
from sqlalchemy import exc, or_, and_
from functools import wraps
import base64
import os
import tarfile
import hashlib
import json

app = Flask(__name__)
app.debug = True

app.config.from_pyfile('repo.cfg')
db = SQLAlchemy(app)

def requires_login(func):
    def _decorator(*args, **kwargs):
        if not session.get('logged_in'):
            flash('You must be logged in to access that page', 'error')
            return redirect(url_for('login'))
        return func(*args, **kwargs)
    return wraps(func)(_decorator)

class SynoPkg(db.Model):
    __tablename__ = 'pkg'
    filename = db.Column(db.String, primary_key=True)
    package = db.Column(db.String)
    version = db.Column(db.String)
    displayname = db.Column(db.String)
    description = db.Column(db.String)
    md5 = db.Column(db.String)
    size = db.Column(db.Integer)
    qinst = db.Column(db.Boolean)
    depsers = db.Column(db.String)
    deppkgs = db.Column(db.String)
    start = db.Column(db.Boolean)
    maintainer = db.Column(db.String)
    changelog = db.Column(db.String)
    beta = db.Column(db.Boolean)
    arch = db.Column(db.String)
    downloads = db.Column(db.Integer)

    def __init__(self, filename):
        self.filename = filename
        self.arch = 'noarch'
        self.qinst = False
        self.start = True
        self.beta = False
        self.downloads = 0

    def __setattr__(self, name, value):
        if name == 'arch' and value is None:
            value = 'noarch'
        db.Model.__setattr__(self, name, value)

class SynoDevice(db.Model):
    __tablename__ = 'device'
    address = db.Column(db.String, primary_key=True)
    uniqid = db.Column(db.String)
    arch = db.Column(db.String)
    major = db.Column(db.String)
    minor = db.Column(db.String)
    build = db.Column(db.String)
    channel = db.Column(db.String)
    lang = db.Column(db.String)
    tz = db.Column(db.String)
    last_response = db.Column(db.String)

    def __init__(self, address, uniqid, arch, major, minor, build, channel, lang, tz):
        self.address = address
        self.uniqid = uniqid
        self.arch = arch 
        self.major = major
        self.minor = minor
        self.build = build
        self.channel = channel
        self.lang = lang
        self.tz = tz

class SimpleConfigParser():
 
    def __init__(self, option_char = '=', strip_quotes = True):
        self.option_char = option_char
        self.strip_quotes = True
 
    def parse_config(self, fp):
        self.options = {}
        for line in fp:
            if self.option_char in line:
                option, value = line.split(self.option_char, 1)
                option = option.strip()
                value = value.strip().strip('"\'')
                self.options[option] = value
        return self.options

def md5sum(fn, block_size=1024):
    md5 = hashlib.md5()
    c = 0
    with open(fn,"rb") as f:
        for chunk in iter(lambda: f.read(1024*block_size), b""):
            md5.update(chunk)
            c = c + 1024*block_size
    return md5.hexdigest()

@app.route('/')
def index():
    return redirect('https://bitbucket.org/polandj')

@app.route('/', methods=['POST'])
def synology_check():
    arch = request.form.get('arch')
    if arch:
        arch = arch.startswith('88f628') and '88f6281' or arch
        arch = arch.startswith('88f528') and '88f5281' or arch
        arch = arch.startswith('ppc824') and 'ppc824x' or arch
        arch = arch.startswith('ppc853') and 'ppc853x' or arch
        arch = arch.startswith('ppc854') and 'ppc854x' or arch
    try:
        pkgs = db.session.query(SynoPkg).filter(SynoPkg.arch.in_([arch, 'noarch'])).all()
        response = []
        for pkg in pkgs:
            p = {}
            p['package'] = pkg.package
            p['version'] = pkg.version
            p['dname'] = pkg.displayname
            p['desc'] = pkg.description
            p['link'] = url_for('pkg_download', filename=pkg.filename, _external=True)
            p['md5'] = pkg.md5
            p['icon'] = url_for('pkg_icon', filename=pkg.filename, _external=True) + '.png'
            p['size'] = pkg.size
            p['qinst'] = False
            p['depsers'] = None
            p['deppkgs'] = None
            p['start'] = True
            p['maintainer'] = pkg.maintainer
            p['changelog'] = pkg.changelog
            p['beta'] = False
            p['arch'] = pkg.arch
            response.append(p)
    except Exception, e:
        app.logger.warning('Error generating packages: %s' % e)
        return 'Package error'

    try:
        # Log the device
        device = SynoDevice(request.access_route[0], request.form.get('unique'), request.form.get('arch'), 
                request.form.get('major'), request.form.get('minor'), request.form.get('build'), 
                request.form.get('package_update_channel'), request.form.get('language'), 
                request.form.get('timezone'))
        device.last_response = json.dumps(response)
        db.session.merge(device)
        db.session.commit()
        return device.last_response
    except Exception, e:
        app.logger.warning('Error saving device: %s' % e)
        return json.dumps(response)

# http://stackoverflow.com/questions/11817182/uploading-multiple-files-with-flask
@app.route('/pkg/new', methods=['GET', 'POST'])
@requires_login
def pkg_upload():
    if request.method == 'POST':
        fh = request.files['file']
        if fh and fh.filename.lower().endswith('.spk'):
            filename = secure_filename(fh.filename)
            fullpath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            fh.save(fullpath)
            pkg = SynoPkg(filename)
            # Get what we can from within the SPK
            pkgtar = tarfile.open(fullpath)
            info = SimpleConfigParser().parse_config(pkgtar.extractfile("INFO"))
            for k,v in info.iteritems():
                for column in SynoPkg.__table__.columns._data.keys():
                    if k == column:
                        setattr(pkg, k, v)
            # Check for icon
            if 'package_icon' in info:
                icon_data = base64.b64decode(info.get('package_icon'))
                with open(fullpath + ".png", 'wb') as f:
                    f.write(icon_data)
            # Check for outer icon
            try:
                icon = pkgtar.extractfile("PACKAGE_ICON.PNG")
                with open(fullpath + ".png", 'wb') as f:
                    f.write(icon.read())
            except KeyError:
                pass
            # And what we can from the file itself
            pkg.size = os.path.getsize(fullpath)
            pkg.md5 = md5sum(fullpath)
            try:
                db.session.merge(pkg)
            except exc.IntegrityError:
                pass
            db.session.commit()
            # redir to edit page
            return redirect(url_for('pkg_edit', filename=filename))
    return render_template('pkg_upload.html')

@app.route('/pkg/delete/<filename>')
@requires_login
def pkg_delete(filename):
    pkg = db.session.query(SynoPkg).filter(SynoPkg.filename == filename).first()
    if pkg:
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename + '.png'))
        db.session.delete(pkg)
        db.session.commit()
        return redirect(url_for('pkg_list'))
    else:
        return 'No such file', 404

@app.route('/pkg/edit/<filename>', methods=['GET', 'POST'])
@requires_login
def pkg_edit(filename):
    pkg = db.session.query(SynoPkg).filter(SynoPkg.filename == filename).first()
    if pkg:
        if request.method == 'POST':
            pkg.changelog = request.form.get('form-changelog')
            db.session.commit()
        return render_template('pkg_edit.html', pkg=pkg)
    else:
        return 'No such file', 404

@app.route('/pkg')
@requires_login
def pkg_list():
    pkgs = db.session.query(SynoPkg).order_by(SynoPkg.package, SynoPkg.version, SynoPkg.arch).all()
    return render_template('pkg_list.html', pkgs=pkgs)

@app.route('/pkg/download/<filename>')
def pkg_download(filename):
    pkg = db.session.query(SynoPkg).filter(SynoPkg.filename == filename).first()
    if pkg:
        pkg.downloads += 1
        db.session.commit()
        return send_from_directory(app.config['UPLOAD_FOLDER'],
                                   filename)
    else:
        return 'No such file', 404

@app.route('/pkg/icon/<filename>')
def pkg_icon(filename):
    if not filename.endswith('.png'):
        filename += '.png'
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/device')
@requires_login
def device_list():
    devices = db.session.query(SynoDevice).order_by(SynoDevice.arch).all()
    return render_template('device_list.html', devices=devices)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if (request.form.get('username') == os.environ['OPENSHIFT_POSTGRESQL_DB_USERNAME'] and
           request.form.get('password') == os.environ['OPENSHIFT_POSTGRESQL_DB_PASSWORD']):
            session['logged_in'] = True
            return redirect(url_for('pkg_list'))
    return render_template('login.html')

@app.route('/logout')
@requires_login
def logout():
    session.clear()
    return redirect(url_for('index'))

@app.route('/initdb')
@requires_login
def initdb():
    db.drop_all()
    db.create_all()
    return 'Database (re)initialized'

if __name__ == '__main__':
    app.run()
